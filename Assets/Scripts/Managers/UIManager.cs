﻿using KnifeHit.Constant;
using KnifeHit.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace KnifeHit.Manager
{
    public class UIManager : MonoBehaviour
    {
        [Header("Ref")]
        [SerializeField] public Transform knifeContainer;
        [SerializeField] private GameObject endScreen;
        [SerializeField] private Text scoreText;
        [SerializeField] private Text message;
        [SerializeField] private GameManager gameManager;
        [SerializeField] private List<Image> knifesRemaning;
        [Space]
        [Header("Config")]
        [SerializeField] public Color usedColor;
        [SerializeField] public Color unUsedColor;
        private LevelData levelData => LevelDataScriptableObj.Instance.GetCurrentLevelData();

        private void Start()
        {
            Reset();
        }
        public void ShowEndScreen(string msg)
        {
            endScreen.SetActive(true);
            message.text = msg;
        }
        public void Retry()
        {
            endScreen.SetActive(false);
            gameManager.Reset();
            Reset();
        }

        private void Reset()
        {
            if (knifesRemaning == null)
                knifesRemaning = new List<Image>();
            var count = knifesRemaning.Count;
            var knifeNeeded = levelData.noOfKnifes;
            var diff = Mathf.Clamp(knifeNeeded - count,0,int.MaxValue);
            for (int i = 0; i < diff; i++)
            {
                var img=Instantiate(knifesRemaning[0],GameConstants.VECTOR3ZERO,Quaternion.identity, knifeContainer).GetComponent<Image>();
                knifesRemaning.Add(img);
            }
            count = knifesRemaning.Count;
            for (int i = 0; i < count; i++)
            {
                knifesRemaning[i].gameObject.SetActive(i <= knifeNeeded);
                knifesRemaning[i].color = unUsedColor;
            }
        }
        public void UpdateScore(int score)
        {
            scoreText.text = score.ToString();
            
        }

        public void UpdateRemaingKnife(int knifeUsed)
        {
            var count = Mathf.Clamp(knifeUsed, 0, knifesRemaning.Count);
            for (int i = 0; i < count; i++)
            {
                knifesRemaning[i].color = usedColor;
            }
        }
        public void Back()
        {
            SceneManager.LoadScene(GameConstants.LOADING_SCENE);
        }

        private void Sound()
        {

        }
    }
}
