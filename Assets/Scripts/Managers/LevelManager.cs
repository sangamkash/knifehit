﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using KnifeHit.Data;
using UnityEngine.SceneManagement;
using KnifeHit.Constant;

namespace KnifeHit.Manager.Level
{
    public class LevelManager : MonoBehaviour
    {
        [SerializeField] Dropdown levelDropdown;
        public LevelDataScriptableObj levelData;
        private void Start()
        {
            var datas=LevelDataScriptableObj.Instance.LevelDatas;
            List<string> options = new List<string>();
            for (int i = 0; i < datas.Length; i++)
            {
                options.Add(datas[i].levelName);
            }
            levelDropdown.AddOptions(options);

        }

        public void OnLevelChanged(int level)
        {
            LevelDataScriptableObj.currentLevel = level;
        }

        public void LoadGame()
        {
            SceneManager.LoadScene(GameConstants.GAMEPLAY_SCENE);
        }

    }
}
