﻿using KnifeHit.Constant;
using KnifeHit.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KnifeHit.Manager
{
   
    public class GameManager : MonoBehaviour
    {
        [Header("ref")]
        [SerializeField] UIManager uIManager;
        [SerializeField] ScoreManager scoreManager;
        [SerializeField] BoardRotator boardRotator;
        [SerializeField] GameObject knifePrefab;
        [SerializeField] Transform lunchPoint;
        public Transform boardTranfrom;
        private Knife knife;
        public int knifeRemaing;
        public int bonusPoint;
        private bool blockInput;
        private LevelData levelData => LevelDataScriptableObj.Instance.GetCurrentLevelData();
        private void Start()
        {
            boardTranfrom = boardRotator.GetComponent<Transform>();
            boardRotator.StartRotating(levelData.rotationData,levelData.boardSprite,levelData.noOfFruits,levelData.noOfFaultKnife);
            knifeRemaing = 0;
            bonusPoint = 0;
            InstantiateKnife();
        }

        private void Update()
        {
            if (blockInput)
                return;
            if(Input.GetMouseButtonDown(0))
            {
                InstantiateKnife();
            }
        }

        private void StopGame()
        {
            Time.timeScale = 0;
        }

        private void ShowEnd(bool isSuccess)
        {
            var msg = isSuccess ? string.Format(GameConstants.SUCCESS_MESSAGE,bonusPoint) : GameConstants.FAIL_MESSAGE;
            uIManager.ShowEndScreen(msg);
        }
        private void InstantiateKnife()
        {
            if (knife != null)
            {
                blockInput = true;
                knife?.Shoot();
            }
            var gameObj = Instantiate(knifePrefab, lunchPoint.position, Quaternion.identity);
            knife = gameObj.GetComponent<Knife>();
            knife.Init(KnifeCollision,levelData.knifeSprite);
        }

        private void KnifeCollision(Transform collisionObj,Vector3 hitPoint,Knife knife)
        {
            blockInput = false;
            var _knifeContainer = boardRotator.GetKnifeContainer();
            var _dir = hitPoint - boardTranfrom.position;
            var _position = _knifeContainer.position+_dir.normalized * (boardRotator.colliderRadius +GameConstants.KNIFE_OFFSET);
            knife.AttackToBoard(_knifeContainer, _position);
            knifeRemaing++;
            switch (collisionObj.tag)
            {
                case GameConstants.BOARD_TAG:
                    knife.PlayEffect();
                    scoreManager.IncremementScore(false);
                    break;
                case GameConstants.FRIUT_TAG:
                    Destroy(collisionObj.gameObject);
                    bonusPoint++;
                    break;
                case GameConstants.KNIFE_TAG:
                    ShowEnd(false);
                    break;
                default:
                    break;
            }
            uIManager.UpdateRemaingKnife(knifeRemaing);
            if(knifeRemaing == levelData.noOfKnifes)
            {
                ShowEnd(true);
                blockInput = true;
                boardRotator.StopRotation();
            }
        }

        public void Reset()
        {
            knifeRemaing = 0;
            bonusPoint = 0;
            scoreManager.ResetScore();
            boardRotator.Reset();
            blockInput = false;
            boardRotator.StartRotating(levelData.rotationData, levelData.boardSprite, levelData.noOfFruits, levelData.noOfFaultKnife);
        }
    }
}
