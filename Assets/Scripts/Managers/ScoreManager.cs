﻿using KnifeHit.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KnifeHit.Manager
{
    public class ScoreData
    {
        public int score;
        public int extraPoint;
        public ScoreData()
        {
            score = 0;
        }
    }
    public class ScoreManager : MonoBehaviour
    {
        public ScoreData scoreData;
        public LevelData levelData => LevelDataScriptableObj.Instance.GetCurrentLevelData();

        public  delegate void ScoreDelegate(int score);
        public static event ScoreDelegate onScoreChangeEvent;

        private void Awake()
        {
            scoreData = new ScoreData();
        }
        public void ResetScore()
        {
            scoreData = new ScoreData();
            if (onScoreChangeEvent != null)
                onScoreChangeEvent(0);
        }

        public void IncremementScore(bool bonusPoint)
        {
            scoreData.score++;
            if (bonusPoint)
                scoreData.extraPoint++;
            if (onScoreChangeEvent != null)
                onScoreChangeEvent(0);
        }
    }
}
