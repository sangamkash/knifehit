﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KnifeHit.Constant
{
    public enum ParticleEffectType
    {
        kifeSuccess,
        KifeSuccessBonus,
        KifeFail
    }
    public static class GameConstants
    {
        //Vectors
        public static readonly Vector3 VECTOR3UP = Vector3.up;
        public static readonly Vector3 VECTOR3ZERO = Vector3.zero;
        public static readonly Vector3 VECTOR3ONE = Vector3.one;
        public static readonly Vector3 VECTOR3FORWARD = Vector3.forward;
        //int
        public const float KNIFE_OFFSET = 0.4f;
        //string
        public const string BOARD_TAG = "Board";
        public const string KNIFE_TAG = "Knife";
        public const string FRIUT_TAG = "Friut";
        //SceneName
        public const string LOADING_SCENE = "Loading";
        public const string GAMEPLAY_SCENE = "GamePlay";
        //PlayerPrefs
        public const string SOUND_PLAYER_PREFS = "SoundKey_KnifeHit";
        //Messages
        public const string FAIL_MESSAGE = "<color=red>Failed !! </color>";
        public const string SUCCESS_MESSAGE = "<color=#FFEF31>congratulations</color>\n  level compeleted with bonus point {0}";

    }
}
