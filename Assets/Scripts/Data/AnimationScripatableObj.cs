﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KnifeHit.Data
{
    [CreateAssetMenu(fileName = "AnimPreset", menuName = "KnifeHit/LevelCofigration/AnimationPresets")]
    public class AnimationScripatableObj : ScriptableObject
    {
        public AnimationCurve AnimationCurve;
    }
}
