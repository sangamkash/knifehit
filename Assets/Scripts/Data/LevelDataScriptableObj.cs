﻿using KnifeHit.Manager.Level;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KnifeHit.Data
{
    [Serializable]
    public class LevelData
    {
        public string levelName;
        [Range(0,7)]
        public int noOfKnifes;
        [Range(0, 8)]
        public int noOfFruits;
        [Range(0, 4)]
        public int noOfFaultKnife;
        public RotationConfigScriptableObj rotationData;
        [Header("Image reference")]
        public Sprite backGroundSprite;
        public Sprite boardSprite;
        public Sprite knifeSprite;
    }
    [CreateAssetMenu(fileName = "LevelData", menuName = "KnifeHit/LevelCofigration/Level")]
    public class LevelDataScriptableObj : ScriptableObject
    {
        public LevelData[] LevelDatas;
        public static int currentLevel=0;
        private static LevelDataScriptableObj instance;
        public static LevelDataScriptableObj Instance
        {
            get
            {
                if (instance == null)
                    instance = Resources.Load<LevelDataScriptableObj>("LevelData");
                return instance;
            }
        }

        public LevelData GetCurrentLevelData()
        {
            return LevelDatas[currentLevel];
        }
    }
}