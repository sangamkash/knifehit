﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KnifeHit.Data
{
    [System.Serializable]
    public class RotationData
    {
        public float Speed;
        public float time;
        public AnimationScripatableObj animationPreset;
    }

    [CreateAssetMenu(fileName = "RotationConfig", menuName = "KnifeHit/LevelCofigration/RotationConfig")]
    public class RotationConfigScriptableObj : ScriptableObject
    {
        public RotationData[] datas;
    }
}
