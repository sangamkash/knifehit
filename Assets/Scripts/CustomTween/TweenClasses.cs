﻿using CustomTweens.Manager;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace CustomTweens
{
    internal class TweenCofig
    {
        //Configuration var
        public Transform transform {get; private set; }
        public float speed { get; private set; }
        public float time { get; private set; }
        public AnimationCurve animationCurve { get; private set; }
        public Action OnComplete { get; private set; }
        
        public float timeCounter;
        public bool forceStop { get; private set; }
        public TweenCofig(Transform transform, float speed, float time, AnimationCurve animationCurve, Action OnComplete)
        {
            this.transform = transform;
            this.speed = speed;
            this.time = time;
            this.animationCurve = animationCurve;
            this.OnComplete = OnComplete;
            timeCounter = time;
            forceStop = false;
        }

        public void ForceStop()
        {
            forceStop = true;
        }
    }

  

    internal class CustomTween
    {
        private static List<TweenCofig> processingQueueRotateZ;
        internal static List<TweenCofig> ProcessingQueueRotateZ { get => processingQueueRotateZ; set => processingQueueRotateZ = value; }

        public static TweenCofig DoRotationToZ(Transform transform, float speed, float time, AnimationCurve animationCurve = null, Action OnComplete = null)
        {
            if (ProcessingQueueRotateZ == null)
                ProcessingQueueRotateZ = new List<TweenCofig>();
            var tweenCofig = new TweenCofig(transform, speed, time, animationCurve, OnComplete);
            ProcessingQueueRotateZ.Add(tweenCofig);
            return tweenCofig;
        }
    }

    internal static class Externsion
    {
        public static void StopTween(this TweenCofig tweenCofig)
        {
            tweenCofig.ForceStop();
        }
    }
}
