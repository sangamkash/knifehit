﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngineInternal;

namespace CustomTweens.Manager
{
    public class TweenManager : MonoBehaviour
    {
        public static TweenManager Instance;
        public void Awake()
        {
            Instance = this;
        }
        private static List<TweenCofig> processingQueueRotateZ => CustomTween.ProcessingQueueRotateZ;
        private static List<TweenCofig> removeTween;
        public void Update()
        {
            if (processingQueueRotateZ == null)
                return;
            var count = processingQueueRotateZ.Count;
            for (int i = count - 1; i >= 0; i--)
            {
              
                var _data = processingQueueRotateZ[i];
                if(_data.transform == null)
                {
                    processingQueueRotateZ.RemoveAt(i);
                    return;
                }
                _data.timeCounter = _data.forceStop ? 0f : _data.timeCounter;
                _data.timeCounter -= Time.deltaTime;
                var animationValue = 1f;
                if (_data.animationCurve != null)
                    _data.animationCurve.Evaluate(1f - Mathf.Clamp(_data.timeCounter,0,_data.time) / _data.time);
                if (_data.timeCounter > 0)
                {
                    _data.transform.Rotate(0f, 0f, _data.speed * animationValue * Time.deltaTime);
                }
                else
                {
                    if (_data.timeCounter + Time.deltaTime > 0)
                        _data.transform.Rotate(0f, 0f, _data.speed * animationValue * _data.timeCounter);
                    processingQueueRotateZ.RemoveAt(i);
                    if (!_data.forceStop)
                        _data.OnComplete?.Invoke();
                }
            }
        }
    }
}
