﻿using UnityEngine;
using CustomTweens; // this is a custom tween created for rotation 
using KnifeHit.Data;
using System.Collections.Generic;
using KnifeHit.Constant;

namespace KnifeHit
{
    public class BoardRotator : MonoBehaviour
    {
        [Header("Ref")]
        [SerializeField] private SpriteRenderer spriteRenderer;
        [SerializeField] private Transform knifeContainer;
        [SerializeField] private Transform fruitContainer;
        [SerializeField] private float fruitSpownOffset=2f;
        [Header("Ref-Prefab")]
        [SerializeField] private GameObject fruitsPrefab;
        [SerializeField] private GameObject knifePrefab;
        private Transform thisTransform;
        private RotationConfigScriptableObj rotationData;
        public float colliderRadius { get; private set; }
        
        private TweenCofig tweenCofig;
       
        private void Awake()
        {
            colliderRadius = GetComponent<CircleCollider2D>().radius;
            thisTransform = GetComponent<Transform>();
        }

        [ContextMenu("StartRotation")]
        public void StartRotating(RotationConfigScriptableObj rotationData,Sprite sprite,int noOfFruit=0,int noOfFaultKnife=0)
        {
            if (tweenCofig != null)
            {
                Debug.LogWarning("Already running");
                return;
            }
            spriteRenderer.sprite = sprite;
            this.rotationData = rotationData;
            GenerateFruitsAtRandom(noOfFruit);
            GenerateKnifeAtRandom(noOfFaultKnife);
            StartRotation();
            
        }

        [ContextMenu("StopRotation")]
        public void StopRotation()
        {
            tweenCofig?.StopTween();
            tweenCofig = null;
        }

        public void Reset()
        {
            thisTransform.rotation = Quaternion.identity;
            var countFruit = fruitContainer.childCount;
            for (int i = countFruit - 1; i >= 0; i--)
            {
                Destroy(fruitContainer.GetChild(i).gameObject);
            }
            var countKnife = knifeContainer.childCount;
            for (int i = countKnife - 1; i >= 0; i--)
            {
                Destroy(knifeContainer.GetChild(i).gameObject);
            }
        }
        public Transform GetKnifeContainer() => knifeContainer;

        /// <summary>
        /// this is a recursively called fucntion 
        /// </summary>
        /// <param name="index"></param>
        private void StartRotation(int index=0)
        {
            
            if(index>=rotationData.datas.Length)
            {
                index = 0;
            }
            if(rotationData == null)
            {
#if DEBUG_LOGS
                Debug.LogError("<color=red>Level Cofigurtion cannot be null</color>");
#endif
                return;
            }
#if DEBUG_LOGS
            Debug.LogError($" playing index{index}");
#endif
            var _rotData = rotationData.datas[index];
            tweenCofig = CustomTween.DoRotationToZ(transform, _rotData.Speed, _rotData.time, null, () =>
            {
                StartRotation(++index);
            }
            );
        }

        private void GenerateFruitsAtRandom(int noOfFruit)
        {
            if (noOfFruit <= 0)
                return;
            var maxNumberOfFruits = 16;
            var deltaAngle = 360f / maxNumberOfFruits;
            var listNumbers = new List<int>();
            int number;
            for (int i = 0; i < noOfFruit; i++)
            {
                do
                {
                    number = Random.Range(0, maxNumberOfFruits);
                    if (number % 2 != 0)
                        number += 1;
                } while (listNumbers.Contains(number));
                listNumbers.Add(number);
            }
           
            for (int i = 0; i < noOfFruit; i++)
            {
                var _angleOfProjection = deltaAngle * listNumbers[i] * Mathf.Deg2Rad;
                var _dir = new Vector3(Mathf.Sin(_angleOfProjection), Mathf.Cos(_angleOfProjection), 0f);
                var _position = thisTransform.position+_dir.normalized * (colliderRadius + fruitSpownOffset);
                Instantiate(fruitsPrefab, _position, Quaternion.identity, fruitContainer);
            }
        }

        private void GenerateKnifeAtRandom(int noOfKnife)
        {
            if (noOfKnife <= 0)
                return;
            var maxNumberOfFruits = 16;
            var deltaAngle = 360f / maxNumberOfFruits;
            var listNumbers = new List<int>();
            int number;
            for (int i = 0; i < noOfKnife; i++)
            {
                do
                {
                    number = Random.Range(0, maxNumberOfFruits);
                    if (number % 2 == 0)
                        number += 1;
                } while (listNumbers.Contains(number));
                listNumbers.Add(number);
            }

            for (int i = 0; i < noOfKnife; i++)
            {
                var _angleOfProjection = deltaAngle * listNumbers[i];
                var _angleOfProjectionRad = _angleOfProjection * Mathf.Deg2Rad;
                var _dir = new Vector3(Mathf.Sin(_angleOfProjectionRad), Mathf.Cos(_angleOfProjectionRad), 0f);
                var _position = thisTransform.position + _dir.normalized * (colliderRadius + GameConstants.KNIFE_OFFSET);
                var _knife=Instantiate(knifePrefab, _position, Quaternion.LookRotation(Vector3.forward,-_dir), knifeContainer).GetComponent<Knife>();
              
                _knife.ForceAttackToBoard(_angleOfProjection);
            }
        }
    }
}
