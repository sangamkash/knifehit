﻿using KnifeHit.Constant;
using System;
using System.Collections;
using UnityEditor;
using UnityEngine;

namespace KnifeHit
{
    public class Knife : MonoBehaviour
    {
        [Header("Ref")]
        [SerializeField] private SpriteRenderer spriteRenderer;
        [Header("Config")]
        [SerializeField] private float speed=1f;
        [SerializeField] private AnimationCurve startupAnimation;
        private Transform thisTransform;
        private Rigidbody2D thisRigidbody;
        private Collider2D thisCollider2D;
        private Action<Transform,Vector3, Knife> OnCollision;
        private bool stopUpdating;
        private bool once = false;
        private bool forceAttached = false;
        private void Start()
        {
            if (forceAttached == true)
                return;
            thisTransform = transform;
            thisRigidbody = GetComponent<Rigidbody2D>();
            thisCollider2D = GetComponent<Collider2D>();
            StartCoroutine(PlayStartUpAnimation());
        }

        private IEnumerator PlayStartUpAnimation()
        {
            var animationTime = 0.2f;
            var startTime = Time.time;
            thisTransform.localScale = GameConstants.VECTOR3ONE * 0f;
            while (Time.time-startTime<animationTime)
            {
                thisTransform.localScale = GameConstants.VECTOR3ONE*startupAnimation.Evaluate((Time.time - startTime) / animationTime);
                yield return new WaitForEndOfFrame();

            }
            thisTransform.localScale = GameConstants.VECTOR3ONE;
        }

        public void Init(Action<Transform,Vector3, Knife> OnCollision,Sprite sprite)
        {
            stopUpdating = true;
            spriteRenderer.sprite = sprite;
            this.OnCollision = OnCollision;
        }
        private void Update()
        {
            if (stopUpdating)
                return;
            thisTransform.position += GameConstants.VECTOR3UP *speed * Time.deltaTime;
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (once)
                return;
#if DEBUG_LOG
            Debug.Log(collision.gameObject.name);
#endif
            once = true;
            OnCollision?.Invoke(collision.transform,transform.position, this);
       
        }
        public void Shoot()
        {
            thisCollider2D.enabled = true;
            stopUpdating = false;
        }
        public void PlayEffect( )
        {

        }
        public void AttackToBoard(Transform board,Vector3 position)
        {
            stopUpdating = true;
            thisRigidbody.isKinematic = true;
            thisRigidbody.velocity = GameConstants.VECTOR3ZERO;
            thisTransform.parent = board;
            thisTransform.position = position;

        }

        public void ForceAttackToBoard(float angle)
        {
            forceAttached = true;
            thisTransform = transform;
            thisRigidbody = GetComponent<Rigidbody2D>();
            thisCollider2D = GetComponent<Collider2D>();

            stopUpdating = true;
            thisRigidbody.isKinematic = true;
            thisRigidbody.velocity = GameConstants.VECTOR3ZERO;
            //thisTransform.eulerAngles = GameConstants.VECTOR3FORWARD * angle;
        }
    }
}
